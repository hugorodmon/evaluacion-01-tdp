﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Evaluacion01___TP
{
    class Estrategia : Juegos
    {
        public string plataforma;

        public Estrategia() { }

        public Estrategia(string _genero, string _formato, string _titulo, string _descripcion) : base(_genero, _formato, _titulo, _descripcion) { }

        public Estrategia(string _genero, string _formato, string _titulo, string _descripcion, string _plataforma) : base(_genero, _formato, _titulo, _descripcion)
        {
            this.plataforma = _plataforma;
        }

        public override void caracteristica()
        {
            this.getInfoJuego();
            Console.WriteLine("Plataforma: " + this.plataforma);
        }

        public void setPlataforma(string _plataforma)
        {
            this.plataforma = _plataforma;
        }

        public string getPlataforma()
        {
            return this.plataforma;
        }
    }
}
