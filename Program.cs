﻿using System;

namespace Evaluacion01___TP
{
    class Program
    {
        static void Main(string[] args)
        {
            Estrategia AgeOfEmpiresII = new Estrategia();

            AgeOfEmpiresII.setTitulo("Age Of Empires II HD Edition");
            AgeOfEmpiresII.setGenero("Estrategia en tiempo real");
            AgeOfEmpiresII.setDescripcion("Age of Empires II es un videojuego de estrategia en tiempo real para computadoras personales y fue desarrollado por Ensemble Studios, y distribuido por Microsoft Game Studios para los sistemas operativos Windows. El juego está ambientado en la Edad Media, justo tras la caída del Imperio romano de Occidente y la toma/saqueo de Roma por los visigodos y los vándalos, hasta el Renacimiento");
            AgeOfEmpiresII.setFormato("Digital");
            AgeOfEmpiresII.setPlataforma("Windows Steam");

            AgeOfEmpiresII.caracteristica();

            Console.WriteLine("");

            RPG TLOZ = new RPG();

            TLOZ.setTitulo("The Legend of Zelda: The Wind Waker");
            TLOZ.setGenero("RPG Mundo Abierto");
            TLOZ.setDescripcion("La historia del juego se desarrolla, por primera vez en la serie, en un archipiélago de un vasto océano, cien años después de los acontecimientos relatados en The Legend of Zelda: Ocarina of Time; el jugador controla a Link, quien lucha contra el malvado Ganondorf por el control de una reliquia sagrada conocida como la Trifuerza. En la mayor parte del juego, el personaje navega por el mar, viaja entre islas y atraviesa mazmorras y templos para obtener el poder necesario con el que enfrentar a Ganondorf. Su aventura comienza una vez que su pequeña hermana es secuestrada por un enorme pájaro que arriba a la isla donde el héroe habita; más tarde, Link descubre que el ave es manipulada por Ganondorf.");
            TLOZ.setFormato("Físico");
            TLOZ.setPlataforma("Nintendo GameCube");

            TLOZ.caracteristica();
        }
    }
}
