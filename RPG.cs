﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Evaluacion01___TP
{
    class RPG : Juegos
    {
        public string plataforma;

        public RPG(){}

        public RPG(string _genero, string _formato, string _titulo, string _descripcion) : base(_genero, _formato, _titulo, _descripcion){}

        public RPG(string _genero, string _formato, string _titulo, string _descripcion, string _plataforma) : base(_genero, _formato, _titulo, _descripcion)
        {
            this.plataforma = _plataforma;
        }

        public override void caracteristica()
        {
            Console.WriteLine("Información del juego " + this.titulo);
            Console.WriteLine("Género: " + this.genero);
            Console.WriteLine("Descripción: " + this.descripcion);
            Console.WriteLine("Formato: " + this.formato);
            Console.WriteLine("Plataforma: " + this.plataforma);
        }

        public void setPlataforma(string _plataforma)
        {
            this.plataforma = _plataforma;
        }

        public string getPlataforma()
        {
            return this.plataforma;
        }
    }
}
