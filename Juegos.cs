﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Evaluacion01___TP
{
    abstract class Juegos
    {
        public string genero;
        public string formato;
        public string titulo;
        public string descripcion;

        protected Juegos(){}

        protected Juegos(string _genero, string _formato, string _titulo, string _descripcion)
        {
            this.genero = _genero;
            this.formato = _formato;
            this.titulo = _titulo;
            this.descripcion = _descripcion;
        }

        public abstract void caracteristica();

        public void getInfoJuego()
        {
            Console.WriteLine("Información del juego " + this.titulo);
            Console.WriteLine("Género: " + this.genero);
            Console.WriteLine("Descripción: " + this.descripcion);
            Console.WriteLine("Formato: " + this.formato);
        }

        public void setGenero(string _genero)
        {
            this.genero = _genero;
        }

        public void setFormato(string _formato)
        {
            this.formato = _formato;
        }

        public void setTitulo(string _titulo)
        {
            this.titulo = _titulo;
        }

        public void setDescripcion(string _descripcion)
        {
            this.descripcion = _descripcion;
        }

        public string getGenero()
        {
            return this.genero;
        }

        public string geFormato()
        {
            return this.formato;
        }

        public string getTitulo()
        {
            return this.titulo;
        }

        public string getDescripcion()
        {
            return this.descripcion;
        }
    }
}
